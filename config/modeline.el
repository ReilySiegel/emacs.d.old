;;; modeline --- Configuration for the modeline.

;; Modeline faces

(make-face 'mode-line-ovwt-face)

(set-face-attribute
 'mode-line-ovwt-face nil
 :inherit 'mode-line-face
 :box '(:line-width 2 :color "#FF0000"))

;; Modeline functions

(defvar modeline-buffer-name
  '(:eval (propertize "%b"
		      'face (if (bound-and-true-p overwrite-mode)
				'mode-line-ovwt-face
			      nil)
		      'help-echo (or (buffer-file-name)
				     "No File"))))

(defvar modeline-buffer-percent
  '(:eval (format "%d%%%%"
		  (round
		   (* 100 (/ (point)
			     (1+ (float (point-max)))))))))



;;; Hooks

(add-hook 'post-command-hook 'redraw-modeline)

;; Set the modeline.

(setq-default
 mode-line-format
 (list
  mode-line-front-space
  modeline-buffer-name
  "    "
  modeline-buffer-percent
  " "
  '(:eval mode-name)
  " "
  mode-line-misc-info))

(provide 'modeline)
