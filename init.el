(require 'package)

(setq package-archives
      '(("GNU ELPA"     . "http://elpa.gnu.org/packages/")
        ("MELPA"        . "https://melpa.org/packages/")
	("ORG ELPA"     . "https://orgmode.org/elpa/")))

(package-initialize)
(setq package-enable-at-startup nil)

(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)

(setq use-package-always-ensure t)

;;; Bad Defaults
(setq inhibit-startup-screen t)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(winner-mode 1)
(show-paren-mode 1)

;; store all backup and autosave files in emacs directory
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))
;;; Custom
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

;; DEPRECATED: use config subtree.
(org-babel-load-file (expand-file-name "~/.emacs.d/myinit.org"))

(defun load-directory (dir)
  (let ((load-it (lambda (f)
		   (load-file (concat (file-name-as-directory dir) f)))))
    (mapc load-it (directory-files dir nil "\\.el$"))))

(load-directory "~/.emacs.d/config")

(require 'init-prog-mode)
(require 'init-markdown)
